var retailer_config={
		"protocol" : "http",
		"port" : "5000",
		"domain" : "localhost",
}

var sellerOne_config={
		"protocol" : "http",
		"port" : "5001",
		"domain" : "localhost",
}

var sellerTwo_config={
		"protocol" : "http",
		"port" : "5002",
		"domain" : "localhost",
}


/*
 * Creating the URI's
 */

function getretailerURI(){
	return retailer_config.protocol +"://"+ retailer_config.domain +":"+ retailer_config.port +"/";
}

function getsellerOneURI(){
	return sellerOne_config.protocol +"://"+ sellerOne_config.domain +":"+ sellerOne_config.port +"/";
}

function getsellerTwoURI(){
	return sellerTwo_config.protocol +"://"+ sellerTwo_config.domain +":"+ sellerTwo_config.port +"/";
}
