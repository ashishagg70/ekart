
//-----------------------URI-----------------------

var retailerURI = getretailerURI();
var sellerOneURI = getsellerOneURI();
var sellerTwoURI = getsellerTwoURI();

// ----------------------CONFIGURING THE CONTROLLERS------------------------



// Checkout controller 

ekart.controller('checkoutController', function ($scope, $http, $cookies , $rootScope , $timeout , $window) {

	$scope.checkoutCartList = {};
	$scope.checkoutCartList.totalPrice = null;
	$scope.cartListforPayment = [];

	$scope.message = null;

	// Get the product details from the Cart
	$scope.viewCheckoutUserCart = function () {
		if ($cookies.get('loginUser') != null) {
			$http.get("http://localhost:8100/" + $cookies.get('loginUser') + '/checkout').then(function (response) {
				$scope.checkoutCartList = response.data;
				$scope.cartListforPayment = $scope.checkoutCartList.productlist;
				$scope.viewCheckoutUserCartmessage = null;
				if($scope.checkoutCartList.productlist.length == 0){
					$scope.viewCheckoutUserCartmessage = ' No items to display ';
				}
			}, function (response) {
				$scope.checkoutCartList = null;
				$scope.viewCheckoutUserCartmessage = response.data;
			});
		} else if($cookies.get('loginUser') == null){
			$window.location.assign("#/login");
		}
	}

	// ADDERESS

	$scope.addressObject = {};
	$scope.addressObject.address = null;
	$scope.addressObject.city = null;
	$scope.addressObject.state = '';
	$scope.addressObject.pinCode = null;
	$scope.addressObject.phoneNumber = null;
	$scope.addressList = [];

	$scope.statesList = ['Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu and Kashmir', 'Jharkhand',
		'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh',
		'Uttarakhand', 'West Bengal'];

	// Adding a new address

	$scope.saveaddress = function () {
		$scope.addressObject.pinCode = Number($scope.addressObject.pinCode);
		$scope.addressObject.phoneNumber = String($scope.addressObject.phoneNumber);
		var addressJsondata = angular.toJson($scope.addressObject);
		$http.post("http://localhost:4003/" + $cookies.get('loginUser') + '/address/add', addressJsondata).then(function (response) {
			$scope.saveaddressmessage = response.data.message;
			$scope.getAddressInfo();
			$timeout(function() {
     		 $scope.saveaddressmessage = null;
    		}, 5000);			
		}, function (response) {
			$scope.saveaddressmessage = response.data.message;
		});
	}

	// Modifying the address

	$scope.modifyaddress = function (addressId) {
		$scope.addressObject.pinCode = Number($scope.addressObject.pinCode);
		$scope.addressObject.phoneNumber = String($scope.addressObject.phoneNumber);
		var addressJsondata = angular.toJson($scope.addressObject);
		$http.post("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId + '/modify', addressJsondata).then(function (response) {
			$scope.modifyaddressmessage = response.data.message;
			$scope.getAddressInfo();
			$timeout(function() {
     		 $scope.modifyaddressmessage = null;
    		}, 5000);
		}, function (response) {
			$scope.modifyaddressmessage = response.data.message;
		});
	}

	// Getting address details

	$scope.getAddressInfo = function () {
		// userId to be changed	
		console.log("inside address")
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address').then(function (response) {
			$scope.addressList = response.data;
			$scope.getAddressInfomessage = null;
			if ($scope.addressList.length == 0) {
				$scope.getAddressInfomessage = 'No address details present';
			}
		}, function (response) {
			$scope.getAddressInfomessage = response.data;
		});
	}

	// Getting address details by addressId : for payment purpose
	

	$scope.getAddressbyIdforPayment = function (addressId) {
		// userId to be changed	
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId).then(function (response) {
			$scope.placeOrderForm.addressDetails.address = response.data[0].address;
			$scope.placeOrderForm.addressDetails.city = response.data[0].city;
			$scope.placeOrderForm.addressDetails.state = response.data[0].state;
			$scope.placeOrderForm.addressDetails.pinCode = response.data[0].pinCode;
			$scope.placeOrderForm.addressDetails.phoneNumber = response.data[0].phoneNumber;
			$scope.message = null;
			if ($scope.placeOrderForm.addressDetails == null) {
				$scope.message = 'No address details present';
			}
		}, function (response) {
			$scope.message = response.data.message;
		});
	}

	// Getting address details by addressId

	$scope.getAddressbyId = function (addressId) {
		// userId to be changed	
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId).then(function (response) {
			$scope.addressObject = response.data[0];
			$scope.message = null;
			if ($scope.addressObject == null) {
				$scope.message = 'No address details present';
			}
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.message;
		});
	}



	// Deleting address details


	$scope.deleteAddressInfo = function (addressId) {
		// userId to be changed	
		console.log("error1");
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId + '/delete').then(function (response) {
			$scope.message = response.data.message;			
			$scope.getAddressInfo();
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.message;
		});
	};



	// // CARD

	$scope.cardInfo = {};
	$scope.cardsList = null;
	$scope.cardInfo.cardNumber = null;
	$scope.cardInfo.nameOnCard = null;
	$scope.cardInfo.expiryMonth = '';
	$scope.cardInfo.expiryYear = '';

	$scope.message = null;

	$scope.addCardInfo = function () {
		$scope.cardInfo.expiryMonth = Number($scope.cardInfo.expiryMonth);
		$scope.cardInfo.expiryYear = Number($scope.cardInfo.expiryYear);
		var cardJsondata = angular.toJson($scope.cardInfo);
		$http.post("http://localhost:8001/" + $cookies.get('loginUser') + '/card/add', cardJsondata).then(function (response) {
			$scope.addCardInfomessage = response.data.data;
			$scope.getCardInfo();
			$scope.cardInfo = {};
			$timeout(function() {
     		 $scope.addCardInfomessage = null;
    		}, 5000);
		}, function (response) {
			$scope.addCardInfomessage = response.data.data;
		});
	}

	// Getting card details

	$scope.getCardInfo = function () {
		$http.get("http://localhost:8001/" + $cookies.get('loginUser') + '/cards').then(function (response) {
			$scope.cardsList = response.data;
			$scope.getCardInfomessage = null;
			if ($scope.cardsList.length == 0) {
				$scope.getCardInfomessage = 'No cards details present';
			}
			$timeout(function() {
     		 $scope.getCardInfomessage = null;
    		}, 5000);
		}, function (response) {
			$scope.getCardInfomessage = response.data;
		});
	}

	// Deleting card details 

	$scope.deleteCardInfo = function (cardNumber) {
		// userId to be changed	
		$http.get("http://localhost:8001/" + $cookies.get('loginUser') + '//* card *//' + cardNumber + '/delete').then(function (response) {
			$scope.deleteCardInfomessage = response.data.message;
			$scope.getCardInfo();
			$timeout(function() {
     		 $scope.deleteCardInfomessage = null;
    		}, 5000);
		}, function (response) {
			$scope.deleteCardInfomessage = response.data.message;
		});
	};


	$scope.placeOrderForm = {};

	$scope.Item = {};
	$scope.Item.productName = null;
	$scope.Item.category = null;
	$scope.Item.sellerName = null;
	$scope.Item.quantity = null;
	$scope.Item.totalPrice = null;

	$scope.placeOrderForm.orderItems = [];
	$scope.placeOrderForm.addressDetails = null;
	$scope.placeOrderForm.paymentMethod = null;

	$scope.placeOrderForm.addressDetails = {};


	// Step 3 : Getting the payment method details 

	$scope.placeOrderForm.paymentMethod = {};
	$scope.placeOrderForm.paymentMethod.cardNumber = null;
	$scope.placeOrderForm.paymentMethod.nameOnCard = null;
	$scope.placeOrderForm.paymentMethod.expiryMonth = '';
	$scope.placeOrderForm.paymentMethod.expiryYear = '';

	$scope.selectPaymentMethod = function (cdnum, name, month, year) {
		$scope.placeOrderForm.paymentMethod.cardNumber = cdnum;
		$scope.placeOrderForm.paymentMethod.nameOnCard = name;
		$scope.placeOrderForm.paymentMethod.expiryMonth = month;
		$scope.placeOrderForm.paymentMethod.expiryYear = year;
	}


	$scope.placeOrderforUser = function () {

		$scope.cardForm.cvv = null;

		for (i = 0; i < $scope.cartListforPayment.length; i++) {
			$scope.Item = {};
			$scope.Item.productName = $scope.cartListforPayment[i].displayName;
			$scope.Item.category = $scope.cartListforPayment[i].category;
			$scope.Item.sellerName = $scope.cartListforPayment[i].sellerName;
			$scope.Item.quantity = $scope.cartListforPayment[i].quantity;
			$scope.Item.totalPrice = $scope.cartListforPayment[i].total;
			$scope.placeOrderForm.orderItems[i] = $scope.Item;
		}

        if( $scope.placeOrderForm.orderItems.length != 0 && $scope.placeOrderForm.addressDetails.address != null &&  $scope.placeOrderForm.paymentMethod.cardNumber != null ){

		var placeOrderJsonDate = angular.toJson($scope.placeOrderForm);
console.log("==>"+placeOrderJsonDate)
		$http.post("http://localhost:8100/" + $cookies.get('loginUser') + '/orders/add', placeOrderJsonDate).then(function (response) {
			$scope.placeOrderforUsermessage = response.data;
			$rootScope.getCartCount();
			$timeout(function() {
     		 $scope.placeOrderforUsermessage = null;
    		}, 5000);
			$window.location.reload();
			$window.location.assign("#/order");
		}, function (response) {
			$scope.placeOrderforUsermessage = response.data;
		});
		} else {
			$scope.placeOrderforUsermessage = ' Please add the required details before placing the order ';
			$timeout(function() {
     		 $scope.placeOrderforUsermessage = null;
    		}, 5000);
		}
	}
});


// Add new address controller 

ekart.controller('addressCtrl', function ($scope, $http, $cookies, $timeout) {

	$scope.addressObject = {};
	$scope.addressObject.address = null;
	$scope.addressObject.city = null;
	$scope.addressObject.state = '';
	$scope.addressObject.pinCode = null;
	$scope.addressObject.phoneNumber = null;
	$scope.addressList = [];
	$scope.modifyAddressObject = {};


	$scope.statesList = ['Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jammu and Kashmir', 'Jharkhand',
		'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh',
		'Uttarakhand', 'West Bengal'];

	// Adding a new address

	$scope.saveaddress = function () {
		$scope.addressObject.pinCode = Number($scope.addressObject.pinCode);
		$scope.addressObject.phoneNumber = String($scope.addressObject.phoneNumber);
		var addressJsondata = angular.toJson($scope.addressObject);
		$http.post("http://localhost:4003/" + $cookies.get('loginUser') + '/address/add', addressJsondata).then(function (response) {
			$scope.message = response.data.message;
			$scope.getAddressInfo();
			$scope.addressObject = {};
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.message;
		});
	}

	// Modifying the address

	$scope.modifyaddress = function (addressId) {
		$scope.modifyAddressObject.pinCode = Number($scope.modifyAddressObject.pinCode);
		$scope.modifyAddressObject.phoneNumber = String($scope.modifyAddressObject.phoneNumber);
		var addressJsondata = angular.toJson($scope.modifyAddressObject);
		$http.post("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId + '/modify', addressJsondata).then(function (response) {
			$scope.modifyaddressmessage = response.data.message;
			$scope.getAddressInfo();
			$timeout(function() {
     		 $scope.modifyaddressmessage = null;
    		}, 5000);
		}, function (response) {
			$scope.modifyaddressmessage = response.data.message;
		});
	}

	// Getting address details

	$scope.getAddressInfo = function () {
		// userId to be changed	
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address').then(function (response) {
			$scope.addressList = response.data;
			$scope.getAddressInfomessage = null;
			if ($scope.addressList.length == 0) {
				$scope.getAddressInfomessage = 'No address details present';
			}
		}, function (response) {
			$scope.getAddressInfomessage = response.data;
		});
	}

	// Getting address details by addressId

	$scope.getAddressbyId = function (addressId) {
		// userId to be changed	
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId).then(function (response) {
			$scope.modifyAddressObject = response.data[0];
			$scope.message = null;
			if ($scope.modifyAddressObject == null) {
				$scope.message = 'No address details present';
			}
		}, function (response) {
			$scope.message = response.data.message;
		});
	}



	// Deleting address details


	$scope.deleteAddressInfo = function (addressId) {
		// userId to be changed	console.log("error2");
		console.log("error2");
		$http.get("http://localhost:4003/" + $cookies.get('loginUser') + '/address/' + addressId + '/delete').then(function (response) {
			
			$scope.message = response.data.message;
			$scope.getAddressInfo();
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.message;
		});
	};



});

// Adding new cardForm and fetching cards details

ekart.controller('addCard', function ($scope, $http, $cookies, $timeout) {

	$scope.cardInfo = {};
	$scope.cardsList = null;
	$scope.cardInfo.cardNumber = null;
	$scope.cardInfo.nameOnCard = null;
	$scope.cardInfo.expiryMonth = '';
	$scope.cardInfo.expiryYear = '';

	$scope.message = null;

	$scope.addCardInfo = function () {
		$scope.cardInfo.expiryMonth = Number($scope.cardInfo.expiryMonth);
		$scope.cardInfo.expiryYear = Number($scope.cardInfo.expiryYear);
		var cardJsondata = angular.toJson($scope.cardInfo);
		$http.post("http://localhost:8001/" + $cookies.get('loginUser') + '/card/add', cardJsondata).then(function (response) {
			$scope.message = response.data.data;
			$scope.getCardInfo();
			$scope.cardInfo = {};
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.data;
		});
	}

	// Getting card details

	$scope.getCardInfo = function () {
		$http.get("http://localhost:8001/" + $cookies.get('loginUser') + '/cards').then(function (response) {
			$scope.cardsList = response.data;
			$scope.getCardInfomessage = null;
			if ($scope.cardsList.length == 0) {
				$scope.getCardInfomessage = 'No cards details present';
			}
		}, function (response) {
			$scope.getCardInfomessage = response.data;
		});
	}


	// Deleting card details 

	$scope.deleteCardInfo = function (cardNumber) {
		// userId to be changed	
		$http.get("http://localhost:8001/" + $cookies.get('loginUser') + '/card/' + cardNumber + '/delete').then(function (response) {
			$scope.message = response.data.message;
			$scope.getCardInfo();
			$timeout(function() {
     		 $scope.message = null;
    		}, 5000);
		}, function (response) {
			$scope.message = response.data.message;
		});
	};


});

// Controller for displaying the offers 

ekart.controller('OffersforToday', function ($scope, $http) {

	$scope.viewProductsList = null;
	$scope.message = null;
	$scope.viewProductsOffers = function () {
		$http.get(retailerURI + 'deals').then(function (response) {
			$scope.viewProductsList = response.data;
			$scope.message = null;
			if ($scope.viewProductsList.length == 0) {
				$scope.message = 'No offers to display';
			}
		}, function (response) {
			$scope.viewProductsList = null;
			$scope.message = response.data;
		});
	}
});


// View users orders controller : USER

ekart.controller('ViewUserOrderController', function ($scope, $http, $cookies, $window, $rootScope) {

	$scope.status = null;
	$scope.viewOrders = {};
	$scope.viewOrders.orderId = null;
	$scope.viewOrders.productName = null;
	$scope.viewOrders.category = null;
	$scope.viewOrders.price = null;
	$scope.viewOrders.quantity = null;
	$scope.viewOrders.totalPrice = null;
	$scope.viewOrders.orderedDate = null;
	$scope.viewOrders.orderStatus = null;
	$scope.viewOrders.ordersList = null;
	$scope.viewOrders.btnReviewProduct = null;
	$scope.viewOrders.btnReviewSeller = null;
	$scope.viewOrders.btnCancel = null;
	$scope.viewOrders.btnReturn = null;
	$scope.viewOrders.allordersList = null;

	$scope.fetchOrderWithStatus = function (val) {
		$http.get("http://localhost:5001/" + $cookies.get('loginUser') + '/orders').then(function (response) {
			$scope.viewOrders.ordersList = response.data;
			$scope.message = null;
			$scope.status = val;
			if ($scope.viewOrders.ordersList.length == 0) {
				$scope.message = "No Pending Orders";
			}
		}, function (response) {
			$scope.viewOrders.ordersList = null;
			$scope.message = response.data;
		});
	}

	// Cancel Order

	$scope.cancelOrder = function (orderId) {
		$http.post("http://localhost:5001/" + $cookies.get('loginUser') + '/orders/' + orderId + '/cancel').then(function (response) {
			$scope.message = response.data;
			$scope.fetchOrderWithStatus('cancelled');
			$scope.status = 'cancelled';
		}, function (response) {
			$scope.message = response.data;
		});
	}

	// Return Order

	$scope.returnOrder = function (orderId) {
		$http.post("http://localhost:5001/" + $cookies.get('loginUser') + '/orders/' + orderId + '/return').then(function (response) {
			$scope.message = response.data.message;
			$scope.fetchOrderWithStatus('returned');
				console.log($scope.message)
			$scope.status = 'returned';			
		}, function (response) {
			$scope.message = response.data.message;
			console.log($scope.message)
		});
	}

	$scope.productReview = {};
	$scope.productReview.userId = null;
	$scope.productReview.reviewComments = null;
	$scope.productReview.rating = 1;
	$scope.productReview.message = null;
	$scope.product = {};
	$scope.product.productName = null;

	$scope.setProductName = function (productname) {
		$scope.product.productName = productname;
	}

	$scope.reviewProduct = function () {

		$scope.productReview.userId = $cookies.get('loginUser');
		var productJsonData = angular.toJson($scope.productReview);
		$http.post(retailerURI + $scope.product.productName + '/reviewproduct', productJsonData).then(function (response) {
			$scope.productReview.message = response.data;
		}, function (response) {
			$scope.productReview.message = response.data;
		});
	};

	$scope.sellerReview = {};
	$scope.sellerReview.userId = null;
	$scope.sellerReview.reviewComments = null;
	$scope.sellerReview.rating = 1;
	$scope.sellerReview.message = null;

	$scope.seller = {};
	$scope.seller.sellerName = null;

	$scope.setsellerName = function (sellerName) {
		$scope.seller.sellerName = sellerName;
	}



	$scope.reviewSeller = function () {

		$scope.sellerReview.userId = $cookies.get('loginUser');
		var sellerJsonData = angular.toJson($scope.sellerReview);

		$http.post(retailerURI + $scope.seller.sellerName + '/review', sellerJsonData).then(function (response) {
			$scope.sellerReview.message = response.data;
		}, function (response) {
			$scope.sellerReview.message = response.data;
		});
	};



});

ekart.controller('searchCtrl', function ($scope, $http, $window, $rootScope, $routeParams) {

	//$scope.productsList = null;
	$scope.searchCtrlObj = {};
	$scope.searchCtrlObj.message = null;
	$scope.searchProducts = function () {
		$http.get(retailerURI + 'searchproduct/' + $scope.searchProduct).then(function (response) {
			$scope.productsList = response.data;
			$scope.searchCtrlObj.message = null;
			if ($scope.productsList.length == 0) {
				$scope.searchCtrlObj.message = 'No search results';
			}
			$scope.searchProduct = null;
			$window.location.assign("#/search");
		}, function (response) {
			$scope.productsList = null;
			$scope.searchCtrlObj.message = response.data;
		});
	}

});

ekart.controller('viewProductsCatalog', function ($scope, $http, $cookies) {

	$scope.viewProductsList = null;
	$scope.message = null;

	$scope.viewProductsOfSeller = function () {
	console.log("inside product catalog");
	console.log($cookies.get('loginUser'));
		if ($cookies.get('loginUser') == 'sellerOne@gmail.com') {
			$http.get(sellerOneURI + 'products').then(function (response) {
				$scope.viewProductsList = response.data;
				$scope.message = null;				
				if($scope.viewProductsList.length == 0)
				{
					$scope.message = 'No products to display';
				}
			}, function (response) {
				$scope.viewProductsList = null;
				$scope.message = response.data;
			});
		} else if ($cookies.get('loginUser') == 'sellerTwo@gmail.com') {
			$http.get(sellerTwoURI + 'products').then(function (response) {
				$scope.viewProductsList = response.data;
				$scope.message = null;
				if($scope.viewProductsList.length == 0)
				{
					$scope.message = 'No products to display';
				}				
			}, function (response) {
				$scope.viewProductsList = null;
				$scope.message = response.data;
			});
		}
	}

});

ekart.controller('addProduct', function ($scope, $http, $cookies) {

	$scope.List = null;
	$scope.message = null;
	$scope.flag = false;
	$scope.addProductForm = {};
	$scope.addProductForm.displayName = null;
	$scope.addProductForm.shortDesc = null;
	$scope.addProductForm.description = null;
	$scope.addProductForm.category = null;
	$scope.addProductForm.price = null;
	$scope.addProductForm.discount = null;
	$scope.addProductForm.deliveryCharge = null;

	$scope.searchProducts = function () {
		$http.get(retailerURI + 'searchproduct/' + $scope.addProductForm.displayName).then(function (response) {
			$scope.productsList = response.data;
			$scope.searchProductsmessage = null;
			if ($scope.productsList.length == 0) {
				$scope.searchProductsmessage = 'No search results';
			}
		}, function (response) {
			$scope.productsList = null;
			$scope.searchProductsmessage = response.data;
		});
	}

	$scope.searchProductbyName = function (productName) {
		$http.get(retailerURI + 'searchproduct/' + productName).then(function (response) {
			$scope.flag = true;
			$scope.addProductForm = response.data[0];
			$scope.addProductForm.price = null;
			$scope.addProductForm.discount = null;
			$scope.addProductForm.deliveryCharge = null;
			$scope.message = null;
			if ($scope.addProductForm == null) {
				$scope.message = 'No search results';
			}
		}, function (response) {
			$scope.addProductForm = null;
			$scope.message = response.data;
		});
	}

	$scope.addProductDetails = function () {

		$scope.addProductForm.price = Number($scope.addProductForm.price);
		$scope.addProductForm.discount = Number($scope.addProductForm.discount);
		$scope.addProductForm.deliveryCharge = Number($scope.addProductForm.deliveryCharge);

		var sellerProductJSONData = angular.toJson($scope.addProductForm);
			console.log("inside add product2");
console.log($cookies.get('loginUser'));
		if ($cookies.get('loginUser') == 'sellerOne@gmail.com') {
			$http.post(sellerOneURI + 'product/add' , sellerProductJSONData).then(function (response) {
				$scope.message = response.data;
			}, function (response) {
				$scope.message = response.data;
			});
		} else if ($cookies.get('loginUser') == 'sellerTwo@gmail.com') {
			$http.post(sellerTwoURI + 'product/add' , sellerProductJSONData).then(function (response) {
				$scope.message = response.data;
			}, function (response) {
				$scope.message = response.data;
			});
		}

	};


});

ekart.controller('modifyProductCtrl', function ($scope, $http, $cookies, $rootScope, $routeParams) {


	$scope.addProductForm = {};
	$scope.addProductForm.displayName = null;
	$scope.addProductForm.shortDesc = null;
	$scope.addProductForm.description = null;
	$scope.addProductForm.category = null;
	$scope.addProductForm.price = null;
	$scope.addProductForm.discount = null;
	$scope.addProductForm.deliveryCharge = null;

	$scope.message = null;
	$scope.modifyProduct = {};
	$scope.modifyProduct.displayName = $routeParams.productName;

	$scope.modifyProduct.deliveryCharge = null;
	$scope.modifyProduct.price = null;
	$scope.modifyProduct.discount = null;



	// Modifying the product details
	$scope.modifyProductdetails = function () {

		var productJSONObject = {};
		productJSONObject.price = $scope.modifyProduct.price;
		productJSONObject.discount = $scope.modifyProduct.discount;
		productJSONObject.deliveryCharge = $scope.modifyProduct.deliveryCharge;
		var productJSONdata = angular.toJson(productJSONObject);

		if ($cookies.get('loginUser') == 'sellerOne') {
			$http.post(sellerOneURI + 'product/' + $scope.modifyProduct.displayName + '/modify', productJSONdata).then(function (response) {
				$scope.message = response.data;
			}, function (response) {
				$scope.message = response.data;
			});
		} else if ($cookies.get('loginUser') == 'sellerTwo') {
			$http.post(sellerTwoURI + 'product/' + $scope.modifyProduct.displayName + '/modify', productJSONdata).then(function (response) {
				$scope.message = response.data;
			}, function (response) {
				$scope.message = response.data;
			});
		}
	}

	$scope.searchProductbyName = function () {

		var searchproductJSONObject = {};
		searchproductJSONObject.products = [];
		searchproductJSONObject.seller = null;

		

		if ($cookies.get('loginUser') == 'sellerOne') {

			searchproductJSONObject.products[0] = $scope.modifyProduct.displayName;
			searchproductJSONObject.seller = $cookies.get('loginUser');

			var productJSONdata = angular.toJson(searchproductJSONObject);

			$http.post(sellerOneURI + 'searchproducts' , productJSONdata ).then(function (response) {
				$scope.addProductForm = response.data[0];
				$scope.modifyProduct.price = $scope.addProductForm.price;
				$scope.modifyProduct.discount = $scope.addProductForm.discount;
				$scope.modifyProduct.deliveryCharge = $scope.addProductForm.deliveryCharge;
				$scope.message = null;
				if ($scope.addProductForm == null) {
					$scope.message = 'No search results';
				}
				}, function (response) {
					$scope.addProductForm = null;
					$scope.message = response.data;
				});
			} 
			else if ($cookies.get('loginUser') == 'sellerTwo') {

			searchproductJSONObject.products[0] = $scope.modifyProduct.displayName;
			searchproductJSONObject.seller = $cookies.get('loginUser');

			var productJSONdata = angular.toJson(searchproductJSONObject);

			$http.post(sellerTwoURI + 'searchproducts' , productJSONdata ).then(function (response) {
				$scope.addProductForm = response.data[0];
				$scope.modifyProduct.price = $scope.addProductForm.price;
				$scope.modifyProduct.discount = $scope.addProductForm.discount;
				$scope.modifyProduct.deliveryCharge = $scope.addProductForm.deliveryCharge;
				$scope.message = null;
				if ($scope.addProductForm == null) {
					$scope.message = 'No search results';
				}
				}, function (response) {
					$scope.addProductForm = null;
					$scope.message = response.data;
				});
		}
	
	}

});

ekart.controller('viewuserCartBySeller', function ($scope, $http, $window, $cookies, $rootScope, $routeParams, $timeout) {

	$scope.sellerCartList = null;
	$scope.message = null;

	// Get the product details from the Cart
	$scope.viewUserCart = function () {
		if ($cookies.get('loginUser') == 'sellerOne') {
			$http.get(sellerOneURI + 'productsincart').then(function (response) {
				$scope.sellerCartList = response.data;
				$scope.viewUserCartmessage = null;
				if($scope.sellerCartList.length == 0){
					$scope.viewUserCartmessage = 'No items to display';
				}
			}, function (response) {
				$scope.sellerCartList = null;
				$scope.viewUserCartmessage = response.data;
			});
		} else if ($cookies.get('loginUser') == 'sellerTwo') {
			$http.get(sellerTwoURI + 'productsincart').then(function (response) {
				$scope.sellerCartList = response.data;
				$scope.viewUserCartmessage = null;
				if($scope.sellerCartList.length == 0){
					$scope.viewUserCartmessage = 'No items to display';
				}
			}, function (response) {
				$scope.sellerCartList = null;
				$scope.viewUserCartmessage = response.data;
			});
		}
	}

	$scope.selectedCartItem = null;
	$scope.viewSelectedProduct = function(productName,userId){
	$scope.selectedCartItem = {};		
		for(i = 0;i < $scope.sellerCartList.length;i++){
			if( (productName == $scope.sellerCartList[i].displayName) && (userId == $scope.sellerCartList[i].userId) )
			{
				$scope.selectedCartItem = $scope.sellerCartList[i];
			}
		}

	}

	$scope.priceObject = {}
	$scope.priceObject.quantity = null;
	$scope.priceObject.cartOfferPrice = null;

	// Updating the cart offer price
	$scope.updateCartprice = function (uid, productName, offerPrice, qty) {
		$scope.priceObject.quantity = qty;
		$scope.priceObject.cartOfferPrice = offerPrice;
		var jsonPriceData = angular.toJson($scope.priceObject);
		if ($cookies.get('loginUser') == 'sellerOne') {

			$http.post(sellerOneURI + 'updatecart/' + uid + '/' + productName, jsonPriceData).then(function (response) {
				$scope.message = response.data;
				$scope.priceObject.cartOfferPrice = 0;				
			}, function (response) {
				$scope.message = response.data;
			});

		} else if ($cookies.get('loginUser') == 'sellerTwo') {

			$http.post(sellerTwoURI + 'updatecart/' + uid + '/' + productName, jsonPriceData).then(function (response) {
				$scope.message = response.data;
				$scope.priceObject.cartOfferPrice = 0;								
			}, function (response) {
				$scope.message = response.data;
			});

		}

		$timeout(function() {
     		 $scope.message = null;
    	}, 5000);

	}


});

ekart.controller('signUpController', function ($scope, $http, $window, $rootScope, $routeParams) {
	$scope.register = {};

	$scope.register.userId = null;
	$scope.register.name = null;
	$scope.register.password = null;

	$scope.registerMe = function () {
		var obj = JSON.stringify($scope.register)
		console.log($scope.register) //******
		$http.post("http://localhost:4001/" + "signup", obj).
			then(function (response) {
				console.log(response.data);
				$scope.signUpMessage = response.data.message;
			}, function (response) {
				$scope.signUpMessage = response.data.message;
			})
	}

});

ekart.controller('editProfileController', function ($scope, $http, $cookies) {
	$scope.modify = {};
	$scope.modify.name = null;
	$scope.modify.password = null;
	$scope.modifyDetails = function () {
		var obj = JSON.stringify($scope.modify)
		$http.post("http://localhost:4001/" + $cookies.get('loginUser') + "/update", obj).
			then(function (response) {
				$scope.modifyMessage = response.data.message;
			}, function (response) {
				$scope.modifyMessage = response.data.message;
			})
	}
});

// View orders by SELLER 

ekart.controller('orders', function ($scope, $http, $window, $rootScope, $routeParams, $cookies, $timeout) {

	$scope.status = null;
	$scope.viewOrders = {};
	$scope.viewOrders.orderId = null;
	$scope.viewOrders.productName = null;
	$scope.viewOrders.quantity = null;
	$scope.viewOrders.totalPrice = null;
	$scope.viewOrders.orderDate = null;
	$scope.viewOrders.orderStatus = null;

	$scope.viewOrders.ordersList = null;

	$scope.fetchOrderWithStatus = function (val) {
		$http.get(retailerURI + $cookies.get('loginUser') + '/sellerorders').then(function (response) {
			$scope.viewOrders.ordersList = response.data;
			$scope.message = null;
			$scope.status = val;
			if ($scope.viewOrders.ordersList.length == 0) {
				$scope.message = "No Pending Orders";
			}
		}, function (response) {
			$scope.viewOrders.ordersList = null;
			$scope.message = response.data;
		});
	}

	// move to deliver 


	$scope.deliverOrder = function (orderId, userId) {
		$http.post(retailerURI + userId + '/orders/' + orderId + '/deliver').then(function (response) {
			$scope.deliverOrdermessage = response.data;
			$scope.status = 'delivered';
			$scope.fetchOrderWithStatus('delivered');
			$timeout(function() {
     		 $scope.deliverOrdermessage = null;
    		}, 5000);
		}, function (response) {
			$scope.deliverOrdermessage = response.data;
		});
	}

});

// Controller for products recommendedProducts

ekart.controller('recommendedProducts', function ($scope, $http, $cookies) {

	$scope.viewRecommendedProductsList = null;
	$scope.message = null;
	$scope.viewRecommendedProducts = function () {
		// http://localhost:2000/<userid>/recommendations
		$http.get(retailerURI + $cookies.get('loginUser') + '/recommendations').then(function (response) {
			$scope.viewRecommendedProductsList = response.data;
			$scope.message = null;
			if ($scope.viewRecommendedProductsList.length == 0) {
				$scope.message = 'No recommendations to display';
			}
		}, function (response) {
			$scope.viewRecommendedProductsList = null;
			$scope.message = response.data;
		});
	}

});


ekart.controller('cartController', function ($scope, $http, $window, $cookies, $rootScope) {

	$scope.cartList = {};
	$scope.cartList.totalPrice = null;
	$scope.cartList.totalDeliveryCharge = null;
	$scope.cartList.grandTotal = null;

	$scope.message = null;

	// Get the product details from the Cart
	$scope.viewUserCart = function () {
		if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') == null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginUser') + '/cart').then(function (response) {
				$scope.cartList = response.data;
				console.log(response.data)
				$scope.viewUserCartmessage = null;
				if ($scope.cartList.cartList.length == 0) {
					$scope.viewUserCartmessage = 'No cart items to display';
				}
				$rootScope.getCartCount();
			}, function (response) {
				$scope.cartCount = null;
				$scope.viewUserCartmessage = response.data;
			});
		} else if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') != null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginGuest') + '/cart').then(function (response) {
				$scope.cartList = response.data;
				$scope.viewUserCartmessage = null;
				if ($scope.cartList.cartList.length == 0) {
					$scope.viewUserCartmessage = 'No cart items to display';
				}
				$rootScope.getCartCount();
			}, function (response) {
				$scope.cartCount = null;
				$scope.viewUserCartmessage = response.data;
			});
		}else if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') != null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginUser') + '/cart').then(function (response) {
				$scope.cartList = response.data;
				$scope.viewUserCartmessage = null;
				if ($scope.cartList.cartList.length == 0) {
					$scope.viewUserCartmessage = 'No cart items to display';
				}
				$rootScope.getCartCount();
			}, function (response) {
				$scope.cartCount = null;
				$scope.viewUserCartmessage = response.data;
			});
		}
		else if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') == null) {
			$scope.viewUserCartmessage = 'No cart items to display';
			$scope.cartList.cartList = [];
		}
	}

	// Get the count of product from the Cart
	$rootScope.getCartCount = function () {
		if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') == null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginUser') + '/cartcount').then(function (response) {
				$rootScope.cartCount = response.data.numberOfRecords;
				$scope.getCartCountmessage = null;
			}, function (response) {
				$rootScope.cartCount = null;
				$scope.getCartCountmessage = response.data;
			});
		} else if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') != null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginGuest') + '/cartcount').then(function (response) {
				$rootScope.cartCount = response.data.numberOfRecords;
				$scope.getCartCountmessage = null;
			}, function (response) {
				$rootScope.cartCount = null;
				$scope.getCartCountmessage = response.data;
			});
		}else if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') != null) {
			$http.get("http://localhost:5002/" + $cookies.get('loginUser') + '/cartcount').then(function (response) {
				$rootScope.cartCount = response.data.numberOfRecords;
				$scope.getCartCountmessage = null;
			}, function (response) {
				$rootScope.cartCount = null;
				$scope.getCartCountmessage = response.data;
			});
		}
	}

	$scope.cartItem = {};
	$scope.cartItem.productName = null;
	$scope.cartItem.sellerName = null;
	$scope.cartItem.quantity = null;
	$scope.cartItem.cartOfferPrice = null;

	// Modify the cart items
	$rootScope.modifyCartItem = function (productname, seller, qty, cartOfferprice) {

		$scope.cartItem.productName = productname;
		$scope.cartItem.sellerName = seller;
		$scope.cartItem.quantity = qty;
		$scope.cartItem.cartOfferPrice = 0;

		var cartItemJson = angular.toJson($scope.cartItem);

		if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') == null) {
		 $http.post("http://localhost:5002/" + $cookies.get('loginUser') + '/modifycart', cartItemJson).then(function (response) { 
				$scope.modifyCartItemmessage = response.data.message;
				$scope.viewUserCart();
			}, function (response) {
				$scope.modifyCartItemmessage = response.data.message;
			});
		} else if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') != null) {
			$http.post("http://localhost:5002/" + $cookies.get('loginGuest') + '/modifycart', cartItemJson).then(function (response) { 
				$scope.modifyCartItemmessage = response.data.message;
				$scope.viewUserCart();
			}, function (response) {
				$scope.modifyCartItemmessage = response.data.message;
			});
		}
	}


	$scope.checkout = function () {
		$window.location.assign('#/payment');
	}

});


ekart.controller('logoutController', function ($scope, $http, $window, $rootScope, $cookies) {
	$rootScope.logout = function () {
		$cookies.remove('loginUser');
		$cookies.remove('loginGuest');
		$window.location.href = "index.html";
	}
});

ekart.controller('loginController', function ($scope, $http, $window, $rootScope, $routeParams, $cookies) {

	$scope.login = {};
	$scope.login.userId = null;
	$scope.login.guestId = '';
	$scope.login.password = null;
	$scope.message = null;

	$scope.ekartLogin = function () {

		if ($cookies.get('loginGuest') != undefined) {
			$scope.login.guestId = $cookies.get('loginGuest');
		}
		var loginJson = angular.toJson($scope.login);
		$http.post("http://localhost:5008/" + "login", loginJson).
			then(function (response) {
				$scope.message = response.data.message;
				if ($scope.message == 'You have successfully logged in!!!') {
					$cookies.put('loginUser', $scope.login.userId);
					$cookies.remove('loginGuest');					
					$window.location.href = "user.html#/user";
				}
				else if ($scope.message.substr(0, 6) == 'Seller') {
					var sellerArray = $scope.message.split(":");
					var sellername=$scope.message.substr(6, 9);
					console.log("==>"+sellername);
					$cookies.put('loginUser', sellerArray[1]);
					$scope.message = 'You have successfully logged in!!!'										
					$window.location.href = "seller.html#/viewMyProducts";
				}
			}, function (response) {
				$scope.message = response.data.message;
			});
	}

});

// Controller for displaying the product details 

ekart.controller('productCtrl', function ($scope, $http, $window, $rootScope, $routeParams, $cookies ,$timeout) {

	$scope.productDetails = {};
	$scope.priceComparisionDetails = {};
	var productParam = $routeParams.productName;

	$scope.getProductDetails = function () {
		$http.get(retailerURI + productParam + '/details').then(function (response) {
			$scope.productDetails = response.data;
			$scope.message = null;
			if ($scope.productDetails == null) {
				$scope.message = 'No details to display';
			}
		}, function (response) {
			$scope.productDetails = null;
			$scope.message = response.data;
		});
	};

	$scope.getPriceComparison = function (productName) {
		$http.get(retailerURI + productName + '/pricecomparison').then(function (response) {
			$scope.priceComparisionDetails = response.data;
			$scope.message = null;
			if ($scope.priceComparisionDetails.length == 0 || $scope.priceComparisionDetails == null) {
				$scope.message = 'No details to display';
			}
		}, function (response) {
			$scope.priceComparisionDetails = null;
			$scope.message = response.data;
		});
	};

	$scope.changeSeller = function (sellerName) {
		$scope.changeSellermessage = null;
		for (i = 0; i < $scope.priceComparisionDetails.length; i++) {
			if (sellerName == $scope.priceComparisionDetails[i].sellerName) {
				$scope.productDetails.price = $scope.priceComparisionDetails[i].price;
				$scope.productDetails.deliveryCharge = $scope.priceComparisionDetails[i].deliveryCharge;
				$scope.productDetails.discount = $scope.priceComparisionDetails[i].discount;
				$scope.productDetails.seller = $scope.priceComparisionDetails[i].sellerName;
				$scope.productDetails.offerPrice = $scope.priceComparisionDetails[i].offerPrice;
				$scope.changeSellermessage = "Product details changed based on seller";
			}
		}
		$timeout(function() {
     		 $scope.changeSellermessage = null;
    	}, 5000);

	}

	$scope.cart = {};
	$scope.cart.productName = null;
	$scope.cart.sellerName = null;
	$scope.cart.quantity = "1";
	$scope.cart.category = null;

	$scope.addToCart = function () {

		$scope.cart.productName = $scope.productDetails.displayName;
		$scope.cart.sellerName = $scope.productDetails.seller;
		$scope.cart.category = $scope.productDetails.category;
		$scope.cart.quantity = Number($scope.cart.quantity);

		var cartObjJson = JSON.stringify($scope.cart);

		// Login or Guest user details 

		if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') == null) {
			$http.post(retailerURI + 'guest' + "/addtocart", cartObjJson).
				then(function (response) {
					$cookies.put('loginGuest', response.data);
					$scope.addToCartmessage = 'Item added to cart';
					$rootScope.getCartCount();
					$scope.cart.quantity = "1";
				}, function (response) {
					$scope.addToCartmessage = response.data;
				})
		} else if ($cookies.get('loginUser') == null && $cookies.get('loginGuest') != null) {
			$http.post(retailerURI + $cookies.get('loginGuest') + "/addtocart", cartObjJson).
				then(function (response) {
					$scope.addToCartmessage = response.data;
					$rootScope.getCartCount();
					$scope.cart.quantity = "1";					
				}, function (response) {
					$scope.addToCartmessage = response.data;
				})
		} else if ($cookies.get('loginUser') != null && $cookies.get('loginGuest') == null) {
			$http.post(retailerURI + $cookies.get('loginUser') + "/addtocart", cartObjJson).
				then(function (response) {
					$scope.addToCartmessage = response.data;
					$rootScope.getCartCount();
					$scope.cart.quantity = "1";					
				}, function (response) {
					$scope.addToCartmessage = response.data;
				})
		}

		$timeout(function() {
     		 $scope.addToCartmessage = null;
    	}, 5000);

	};

	// Add to wishlist

	$scope.wishlistProduct = {};
	$scope.wishlistProduct.displayName = null;
	$scope.wishlistProduct.shortDesc = null;
	$scope.wishlistProduct.category = null;

	$scope.addtoWishList = function (productName) {
			
			$scope.wishlistProduct.displayName = productName;
			$scope.wishlistProduct.shortDesc = $scope.productDetails.shortDesc;
			$scope.wishlistProduct.category = $scope.productDetails.category;

			var wishProductJson = angular.toJson($scope.wishlistProduct);	
	
		$http.post("http://localhost:5020/" + $cookies.get('loginUser') + '/wishlist/' + productName + '/add', wishProductJson).then(function (response) {
			$scope.addtoWishListmessage = response.data.response;
			$window.location.assign("#/wishlist");
		}, function (response) {
			$scope.addtoWishListmessage = response.data.response;
		});

		$timeout(function() {
     		 $scope.addtoWishListmessage = null;
    	}, 5000);

	}

	// check for logged in user

	$rootScope.checkLoggedIn = function () {

		if ($cookies.get('loginUser') != null) {
			return true;
		} else {
			return false;
		}

	};


});


ekart.controller("wishListController", function ($scope, $http, $cookies) {
	$scope.wishList = null;

	$scope.wishlistProduct = {};
	$scope.wishlistProduct.displayName = null;
	$scope.wishlistProduct.shortDesc = null;
	$scope.wishlistProduct.category = null;

	$scope.addtoWishList = function (productName) {
		var wishProductJson = angular.toJson($scope.wishlistProduct);
		$http.post("http://localhost:5020/" + $cookies.get('loginUser') + '/wishlist/' + productName + '/add', wishProductJson).then(function (response) {
			$scope.message = response.data.response;
			$scope.getProductsfromWishList();
		}, function (response) {
			$scope.message = response.data.response;
		});
	}

	$scope.getProductsfromWishList = function () {
		$http.get("http://localhost:5020/" + $cookies.get('loginUser') + '/wishlist').then(function (response) {
			$scope.wishList = response.data;
			if ($scope.wishList.length == 0) {
				console.log("fgaghdvhassdjksdfjkjk+"+response.data)
				$scope.message = 'No products in your wish list';
			}
		}, function (response) {
			$scope.message = response.data;
		});
	}

	$scope.removeProductFromWishList = function (productName) {
		    $http.post("http://localhost:5020/" + $cookies.get('loginUser') + '/wishlist/' + productName + '/remove').then(function (response) {
			$scope.message = response.data.response;
		
			$scope.getProductsfromWishList();
		}, function (response) {
			$scope.message = response.data.response;
			console.log(response.data)
		});
	}


});

ekart.controller('notificationController', function ($scope, $http, $cookies) {

	$scope.notificationController = {};
	$scope.notificationController.message = null;
	$scope.notificationController.timestamp = null;

	$scope.notificationsList = null;
	$scope.size = null;

	$http.get("http://localhost:4002/" + $cookies.get('loginUser') + '/notifications').then(function (response) {

		$scope.notificationsList = response.data;
		$scope.size = $scope.notificationsList.length;

		if ($scope.notificationsList.length == 0) {
			$scope.notificationController.message = " No notifications to display ";
		}

	}, function (response) {
		$scope.notificationController.message = response.data.message;
	});

});