// -----------------------REGISTERING THE ekart AND DEPENDENCIES-----------------------

var ekart = angular.module("ekart", ['ngRoute', 'ngCookies', 'angular-rating']);

// ----------------------CONFIGURING THE ekart------------------------

ekart.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/order', {
		templateUrl: 'partials/UserViewOrders.html',
		controller: 'ViewUserOrderController'
	}).when('/HomeProducts', {
		templateUrl: 'partials/HomeProductsOffers.html',
		controller: 'OffersforToday'
	}).when('/EditProfile', {
		templateUrl: 'partials/editProfile.html',
		controller: 'editProfileController'
	}).when('/Signup', {
		templateUrl: 'partials/signup.html',
		controller: 'signUpController'
	}).when('/user', {
		templateUrl: 'partials/userRecommends.html',
		controller: 'recommendedProducts'
	}).when('/recommend', {
		templateUrl: 'partials/userRecommends.html',
		controller: 'recommendedProducts'
	}).when('/search', {
		templateUrl: 'partials/searchProduct.html',
		controller: 'searchCtrl'
	}).when('/login', {
		templateUrl: 'partials/login.html',
		controller: 'loginController'
	}).when('/cart', {
		templateUrl: 'partials/cart.html',
		controller: 'cartController'
	}).when('/payment', {
		templateUrl: 'partials/payment.html',
		controller: 'checkoutController'
	}).when('/productDetails/:productName', {
		templateUrl: 'partials/product.html',
		controller: 'productCtrl'
	}).when('/orderStatus', {
		templateUrl: 'partials/orderStatus.html',
		controller: 'orders'
	}).when('/userCart', {
		templateUrl: 'partials/userCart.html',
		controller: 'viewuserCartBySeller'
	}).when('/addProducts', {
		templateUrl: 'partials/addProduct.html',
		controller: 'addProduct'
	}).when('/modifyProduct/:productName', {
		templateUrl: 'partials/modifyProducts.html',
		controller: 'modifyProductCtrl'
	}).when('/viewMyProducts', {
		templateUrl: 'partials/viewCatalogue.html',
		controller: 'viewProductsCatalog'
	}).when('/wishlist', {
		templateUrl: 'partials/wishList.html',
		controller: 'wishListController'
	}).when('/notifications', {
		templateUrl: 'partials/notifications.html',
		controller: 'notificationController'
	}).when('/addr', {
		templateUrl: 'partials/address.html',
		controller: 'addressCtrl'
	}).when('/payMethod', {
		templateUrl: 'partials/payMethod.html',
		controller: 'addCard'
	}).otherwise({
		redirectTo: '/HomeProducts'
	});
}]);

