    var express = require('express'); 
    var app = express(); 
    var bodyParser = require('body-parser');
		
    app.use(function(req, res, next) { //allow cross origin requests
        res.setHeader("Access-Control-Allow-Methods", "POST, PUT, OPTIONS, DELETE, GET");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    /** Serving from the same express Server No cors required */
    app.use(express.static('./app'));
    	
	app.set('port', 5003);
	
    app.listen('5003', function(){
        console.log('Node server running on 5003 port ...');
    });
	
	
	